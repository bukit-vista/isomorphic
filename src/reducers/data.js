import { defaultMemoize } from 'reselect'
import {RECIEVE_API_DATA} from '../actions'

export default (state={}, {type, data}) => {
    switch (type) {
        case RECIEVE_API_DATA:
            return data
        default:
            return state
    } 
}