import React from 'react'

function Button (prop) {
    return (
        <button 
            type={prop.Type}
            className= {prop.Id}
            onClick = {prop.onClick} 
        >
            {prop.Text}
        </button>
    )
}

export default Button;


