import React from 'react'

function Input (prop) {
    return (
        <input
            type = {prop.Type}
            className = {prop.Class}
            autoComplete = {prop.AutoComplete}
            onKeyUp = {prop.OnKeyUp}
            placeholder = {prop.PlaceHolder}
        />
    )
}

export default Input;


