import React from 'react'

function H2 (prop) {
    return (
        <h6
            className={prop.Class}
        >{prop.Text}</h6>
    )
}

export default H2;


