import React, { useState } from 'react'
import Text from './text';
import JsonText from '../../languageProvider/locales/en_US.json'
import Img from './img'

function Content (prop) {
    if(prop.Data){
        return (
            <div className="allLeft">
                <Img 
                    ImgUrl = {prop.Data.imgurl}
                />
                    <br></br>
                    <br></br>
                    <br></br>
                <Text
                    Text_ = {JsonText.card_greetings1.replace("[firstName]", prop.Data.firstName).replace("[lastName]", prop.Data.lastName)}
                />
                    <br></br>
                <Text 
                    Text_ = {JsonText.card_greetings2}
                />
                    <br></br>
                <table className="table">
                    <tr>
                        <td colspan="1">{JsonText.card_inf1}</td>
                        <td colspan="3"><b>{prop.Data.propertyName}</b></td>
                    </tr>
                    <tr>
                        <td colspan="1">{JsonText.card_inf2}</td>
                        <td colspan="1"><b>{prop.Data.checkInDate}</b></td>
                        <td colspan="1">{JsonText.card_inf3}</td>
                        <td colspan="1"><b>{prop.Data.checkOutDate}</b></td>
                    </tr>
                    <tr>
                        <td colspan="1">{JsonText.card_inf4}</td>
                        <td colspan="3">
                            <input 
                                id="tez123"
                                type="time" 
                                value = {prop.Data.arrivalTime}
                                onChange={prop.TimeChange}
                            >
                            </input>
                            <text>
                                {!prop.Data.arrivalTime ? " (please set your arrival time)"  : " Thank you, your host has been informed about your arrival"}
                                
                            </text>
                        </td>
                    </tr>
                </table>
                { 
                    prop.Index==0 ? "" :
                    <button onClick ={prop.Undo} className="bukitButton">
                        undo
                    </button>
                }
                {
                    prop.Index==prop.HistoryLength-1 ? "" :
                    <button onClick ={prop.Redo} className="bukitButton">
                        redo
                    </button>
                }
            </div>
        )
    } else {
        return <text></text>
    }
       
}

export default Content;


