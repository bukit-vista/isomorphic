import React, {useEffect, useState} from "react";
import H6 from '../components/h6'
import Input from '../components/input'
import Text from '../components/text'
import * as jsonText from "../../languageProvider/locales/en_US.json"
import Content from '../components/content'
import Immutable from 'lodash'




function main (prop) {

    // VARS
    let typingTimer = null;
    const [text1, setText1] = useState('')
    const [show, setShow] = useState(false)
    const [helper, setHelper] =useState(false)
    const [index, setIndex] = useState(0)
    const [history,setHistory] = useState([])
    const [singleData, setSingleData] = useState({
        bookingNo: "",
        propertyName: "",
        firstName: "",
        lastName: "",
        arrivalTime: "",
        checkInDate: "",
        checkOutDate: "",
        imgurl: ""
    })

    

    // FUNCTIONS

    // 1.FUNCTION FOR BUTTON UP EVENT
    
    // So basically this function is used to validate input 
    // > fetch then check data from API (exist/no) **
    // > pass booking info to local variable {singleData}
    // > enable/show the card (content) IF Booking Number exists 

    // * I think the best approach is not to proccess this locally, but to send a query to the API, 
    // but I'm still not there yet, I'm still working on it. So for now I use this function to complete my assessment.
    function doneTyping(e) {
        const val = e.target.value;
        clearTimeout(typingTimer);
        typingTimer = setTimeout(() => {
        setShow(false)   
        if(val=="") {                                                           // If empty (upon deletion)
            setText1("")
        } else if (!(/[^A-Z0-9]/.test(val)) && val!=""){                      // ALL CAPS, no space, alphanumerical, no special char
                fetch(jsonText.dataJSON).then(response => response.json())      // Call api function
                    .then((jsonData) => {
                        for(var i=0; i<jsonData.length; i++){
                            if(jsonData[i].bookingNo==val){                     // If Booking Number exists
                                setSingleData(jsonData[i])                      // Save the selected object 
                                setText1("")                                    // Set text to empty
                                setShow(true)   
                                setHistory([...history,jsonData[i]])                                 // Show card main component
                                break
                            }
                            // The dummy API server doesn't support changes, 
                            // in other words, I can't change the JSON on the server
                        }
                        
                    })
                    .catch((error) => {
                        console.error(error)
                    })
                    
                    setHelper(!helper)
                if(!show) {
                    setText1("Booking Number doensn't exists")
                }

            } else { // Check errors
                if (/[a-z]/.test(val)) {setText1("Code should be in capital letter.")}                                          // Lowercase
                else if (/[\s]/.test(val)) {setText1("Code shouldn't include space(s).")}                                       // Space
                else if (/[$&+,:;=?@#|'<>.-^*()%!]/.test(val)) {setText1("Code shouldn't have special character(s).")}          // Special Char
                else if (!(/[^A-za-z0-9\s$&+,:;=?@#|'<>.-^*()%!]/.test(val))) {setText1("Code should only be alphanumeric.")}   // Others
            }           
        }, 300)      
    } 

    // 2. FUNCTION FOR CHANGING DATETIME
    function timeChange(e) {
        var dummyData = {
            bookingNo: "",
            propertyName: "",
            firstName: "",
            lastName: "",
            arrivalTime: "",
            checkInDate: "",
            checkOutDate: "",
            imgurl: ""
        }

        dummyData = Immutable.clone(singleData) // COPY the value of the original data
        dummyData.arrivalTime = e.target.value  // CHANGE arrival time
        var dummy = history.slice(0,index+1)    // REMOVE all the redo value
        setHistory([...dummy, dummyData]) 
        setIndex(index+1)                       // HISTORY ARRAY INDEX
        setSingleData(dummyData)                // UPDATING the original data
        // *SEND NEW CHANGED DATA TO API WITH QUERY* //
    }

    // 3. FUNCTION TO UNDO
    function undo(){
        setIndex(index-1)
    }
    
    // 4. FUNCTION TO REDO
    function redo(){
        setIndex(index+1)
    }
    // REDO AND UNDO IS DONE BY ITERATING THROUGH THE history ARRAY 





    // Can't integrate Hooks&Classes. 
    // I'll try my best to build the code as Modular and as 
    // similar as the original isomorphic app it can be, whilst using the Hooks. 
    return (
        <div className="main center">
            <div className="card">
                    {/* HEADER */}
                    <div className="cardHeader">
                        <H6 
                            Text = {jsonText.main_bookingCode_text}
                            Class = "cardText1"
                        />
                        <Input 
                            Type = "text"
                            Class ="textBox"
                            AutoComplete = "off"
                            Value = {text1}
                            OnKeyUp = {doneTyping}
                            PlaceHolder = {jsonText.main_inputPlaceHolder}
                        />
                        <br></br>
                        <Text
                            Text_ = {text1}
                            Class = "cardText3"
                        />
                    </div>

                    {/* MAIN */}
                    <div className="cardMain">
                        <Content // I'll make the html as similar as 'CARD'
                            TimeChange = {timeChange}
                            Undo = {undo}
                            Redo = {redo}
                            Data = {history[index]}
                            Index = {index}
                            HistoryLength = {history.length}
                        />
                        
                    </div>
            </div>
        </div>
    )
}


export default main;


