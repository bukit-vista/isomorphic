import React from 'react'

function sidebar (prop) {
    return (
        <aside class="sidenav">
            <div>
                <text className="sidenavLogo">{prop.Title}</text>
            </div>
            <hr className="stripe"></hr>
            <section>
                <a href="#" class="sidenavList">{prop.Text_}</a>
            </section>
        </aside>
    )
}

export default sidebar;


