import React from "react"
import * as Text from "../../languageProvider/locales/en_US.json"
import SideBar from "./sidebar"
import Main from "./main"

function Form() {

    
    return (
        <div className="body">
            <SideBar //should be isomorphicSideBar
                Text_={Text.sidebar_text}
                Title={Text.sidebar_title}
            />

            <Main //shouldbe isoContentMainLayout
            />
            
        </div>
    );
}

export default Form;