import React from 'react';
import ReactDOM from 'react-dom';
import App from './customApp/blankPage';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.register();


// I used Hook here, not classes. 
// I'll collect the this file but definitely would still work on the class approach, AND
// if, by anychance, you would want to see the progress on my class approach, 
// it will be on my github repo named 'isoClassApproach' (*if )

// Should have used store. I didn't realize how important it is till I'm working on this assessment.
// Therefore, Immutability is not implemented on this assessment.